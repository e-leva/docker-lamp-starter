#!/bin/sh
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. "$DIR/.env"

RED='\033[0;31m'
NC='\033[0m' # No Color


touch -a ./acme.json
chmod 600 ./acme.json

if !([ -x "$(command -v docker)" ];) then
    echo
    echo ">>> ${RED}Docker risulta non Installato${NC} <<<"
    echo "Sequi le indicazioni alla seguente guida:"
    echo "https://docs.docker.com/engine/install/ubuntu/"
    echo
    echo "Script teminato"
    exit
fi

if !([ -x "$(command -v docker-compose)" ];) then
    echo
    echo ">>> ${RED}Docker Compose risulta non installato${NC} <<<"
    echo "Sequi le indicazioni alla seguente guida:"
    echo "https://phoenixnap.com/kb/install-docker-compose-on-ubuntu-20-04"
    echo
    echo "Script teminato"
    exit
fi

docker network create ${DEFAULT_NETWORK}

docker-compose build
docker-compose up -d

echo "Tutto fatto!!!"
